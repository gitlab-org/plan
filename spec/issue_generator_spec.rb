require 'rspec'
require "active_support"
require 'active_support/testing/time_helpers'
require 'webmock/rspec'
require_relative '../lib/issue_generator'

describe IssueGenerator do
  include ActiveSupport::Testing::TimeHelpers

  let(:milestones) do
    [
      { 'title' => 'Non-release milestone' },
      { 'title' => 'Start Date milestone', 'start_date' => '2023-01-01' },
      { 'title' => 'Due date milestone', 'due_date' => '2023-01-01' },
      { 'title' => '15.2', 'start_date' => '2023-01-01', 'due_date' => '2023-01-31' },
      { 'title' => '15.3', 'start_date' => '2023-02-01', 'due_date' => '2023-02-28' }
    ]
  end

  before do
    stub_request(:get, 'https://gitlab.com/api/v4/groups/9970/milestones?per_page=100&state=active')
      .to_return(status: 200, body: milestones.to_json, headers: { 'Content-Type': 'application/json' })
  end

  describe '#title' do
    context 'when group name is project_management' do
      subject { described_class.new(group_name: 'project_management') }

      it 'generates the correct title' do
        travel_to(Date.new(2023, 1, 15)) do
          expect(subject.title).to eq('Plan:Project Management | 15.3 Release Planning')
        end
      end
    end
  end
end
