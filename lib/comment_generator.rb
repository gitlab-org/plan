class CommentGenerator
  def initialize(template_filename: nil)
    @template_filename = template_filename
  end

  def comment_body_json
    {
      body: comment_body_from_template
    }.to_json
  end

  private

  def comment_body_from_template
    file = File.expand_path("../../.gitlab/comment_templates/#{@template_filename}", __FILE__)

    unless File.exists?(file)
      raise RuntimeError.new("Expected a file called #{@template_filename} to exist, but it doesn't. Try creating it and running again.")
    end

    File.read(file)
  end
end
