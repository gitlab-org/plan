require 'httparty'

class IssueFinder
  def initialize(project_id: nil, issue_type: nil)
    @project_id = project_id
    @issue_type = issue_type
  end

  def issues
    @issues ||= begin
                    url = "https://gitlab.com/api/v4/projects/#{@project_id}/issues#{filters}"
                    headers = { 'PRIVATE-TOKEN' => ENV['PLAN_PROJECT_TOKEN'] }
                    HTTParty.get(url, headers: headers).parsed_response
               end
  end

  private

  def filters
    case @issue_type
    when "weekly_digest"
        "?labels=weekly%20update&state=opened&per_page=1&sort=desc"
    else
        ""
    end
  end

end
