## Summary


This is an issue to organize a quarterly Plan [Team Day](https://about.gitlab.com/handbook/leadership/building-trust/#host-a-team-day) of fun activities! 

### Event Date

TBD - see comment below to vote on a date

### Activities

Time slots throughout the day are open for team-members to self-organize activities! Organize a game, teach a skill, give a talk on something you know, or anything else you think others might enjoy.


#### How to propose a session:

* Claim any free 1hr slot in the table below
  * Subdivide to 2x30 minutes if you need a smaller slot
* Add a comment for your proposed activity, giving a quick description and asking for upvotes from attendees.
* Link to the comment in #s_plan-social so others can vote. 
* If you get the number of upvotes you need to run the session, it's all yours!

#### How to vote on a session:

* Please vote on any comment below that you're **able and willing to attend** with :+1:
* If you aren't sure if you can attend yet but want to show your interest, any other emoji will do! 



| Time Slot (UTC) | Time Slot (PST - UTC-8) | Time Slot (AEST - UTC+10) | Activity | DRI | Zoom link
| --- | --- | --- | --- | --- | ---
| 00:00 | 16:00 (Day 1) | 10:00 |  |  | 
| 01:00 | 17:00 (Day 1) | 11:00 | | |
| 02:00 | 18:00 (Day 1) | 12:00 | | |
| 03:00 | 19:00 (Day 1) | 13:00 | | |
| 04:00 | 20:00 (Day 1) | 14:00 | | |
| 05:00 | 21:00 (Day 1) | 15:00 | | |
| 06:00 | 22:00 (Day 1) | 16:00 | | |
| 07:00 | 23:00 (Day 1) | 17:00 | | |
| 08:00 | 00:00 | 18:00 | | |
| 09:00 | 01:00 | 19:00 | | |
| 10:00 | 02:00 | 20:00 | | |
| 11:00 | 03:00 | 21:00 | | |
| 12:00 | 04:00 | 22:00 | | |
| 13:00 | 05:00 | 23:00 | |  | 
| 14:00 | 06:00 | 00:00 (Day 2) |  |  | 
| 15:00 | 07:00 | 01:00 (Day 2) || | 
| 16:00 | 08:00 | 02:00 (Day 2) |  |   |
| 17:00 | 09:00 | 03:00 (Day 2) | | |
| 18:00 | 10:00 | 04:00 (Day 2) |  |  | 
| 19:00 | 11:00 | 05:00 (Day 2) | | |
| 20:00 | 12:00 | 06:00 (Day 2) | | |
| 21:00 | 13:00 | 07:00 (Day 2) | | |
| 22:00 | 14:00 | 08:00 (Day 2) | | |
| 23:00 | 15:00 | 09:00 (Day 2) | | |
