# Product Shadowing for Engineers

<!--
Change the title to `Product Shadow: <Engineer's name> shadowing <PM's name>`

Please fill in this issue description with the following:
- Start date and end date
- A small (~2 sentence) blurb of what you're hoping to get out of it

Get agreement from the PM and approval from your manager.
-->

## Before shadowing:

- [ ] A PM is assigned to this issue
- [ ] Agree on start date and end date
- [ ] Added small (~2 sentence) blurb of what you're hoping to get out of it
- [ ] Approval from manager

### Itinerary

<!--

Shadowing should last 2 full working days but can be split up to best suit time zones and to expose participants to a more varied workload

For example:

|   | Start Time (UTC) | End Time | Description |
|---|--- |--- |--- |
| Session 1 | 2020-06-30 1200 | 2020-06-30 1800 | Customer call, backlog refinement |

-->

|   | Start Time (UTC) | End Time | Description |
|---|--- |--- |--- |
|   |    |    |    |

## Post-shadowing:

### Retrospective

Create a comment with each of the following as a header for discussion:

  - What went well?
  - What didn't go so well?
  - How to improve going forward?

/assign me
