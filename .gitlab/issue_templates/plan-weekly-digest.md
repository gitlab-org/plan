## Engineering Managers

<!---
SAFE reminder: While this issue is confidential, the project is public. Out of an abundance of caution, do not directly mention unSAFE information here.
--->

- Put any content you'd like to include in the issue:

  - [ ] @blabuschagne
  - [ ] @donaldcook
  - [ ] @johnhope
  - [ ] @kushalpandya

- [ ] [DRI EM](https://about.gitlab.com/handbook/engineering/development/dev/plan/#dris): Make sure all content is present by Thursday (it is your responsibility, other EMs are just helping)

## :writing_hand:  Stage Updates


## :globe_with_meridians:  Company Updates


## :dancers: Team Members Availability


## :sun_with_face: Family & Friends Days

Upcoming Family and Friends Days (See [the handbook page](https://about.gitlab.com/company/family-and-friends-day/)):

- **2023-09-22**
- 2023-10-06
- 2023-11-27
- 2023-12-29
- 2024-01-12

## :lifter: Training


## :link: Links


## :speech_balloon: Opened discussions


## :headphones: Opportunities

GitLab provides programmes and reimbursements to help you improve your skills, travel and tailor your workspace to work for you. This section highlights some of the existing and new facilities available to you.

### New

* _No additions/changes recently_
### Recurring

* [Growth and Development Benefit](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit)
* [Your work space](https://about.gitlab.com/handbook/finance/procurement/office-equipment-supplies/)
* [General Expenses](https://about.gitlab.com/handbook/finance/expenses/#expense-policy-outline) (subject to restrictions, see the linked table):
  * Books
  * Co-Working Space
  * Conferences
  * Office Equipment
  * Office Supplies
  * VPN
* [Shadow a PM](https://about.gitlab.com/handbook/engineering/development/dev/plan/#product-shadowing-schedule)
* [O'Reilly Learning](https://about.gitlab.com/handbook/people-group/learning-and-development/self-paced-learning/#oreilly-learning)

## :book: Documents to check weekly

- [Engineering Week In Review](https://docs.google.com/document/d/1JBdCl3MAOSdlgq3kzzRmtzTsFWsTIQ9iQg0RHhMht6E/edit)
- [Plan Weekly Meeting Agenda](https://docs.google.com/document/d/1cbsjyq9XAt9UYLIxDq5BYFk47VA5aaTeHfkY2dttqfk/edit)


/assign @blabuschagne @donaldcook @johnhope @kushalpandya
/due Thursday
/label "weekly update" "section::dev" "devops::plan"
/confidential
