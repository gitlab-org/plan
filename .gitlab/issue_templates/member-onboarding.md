## Welcome to the Plan Stage Team!

This issue will be used to track specific Plan related onboarding tasks

<details>
<summary>Manager Tasks</summary>

1. [ ] Add the new team member to the #s_plan, #s_plan-social, and #g_[group name] slack channels.
1. [ ] Add the new team member to the #s_plan_standup slack channel, which will add the member to Geekbot.
1. [ ] Add the new team member to the two Plan weekly meetings.
1. [ ] Invite the new team member to [Plan project](https://gitlab.com/gitlab-org/plan/-/project_members) as a maintainer.
1. [ ] Invite the new team member to [Plan Stage group](https://gitlab.com/groups/gitlab-org/plan-stage/-/group_members) as a maintainer.
1. [ ] Invite the new team member to [team retrospectives project](https://gitlab.com/gl-retrospectives/plan/-/project_members) as a maintainer.
1. [ ] Add the new team member to the [async retro team list](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml).
1. [ ] Create and share onboarding guide ([recommended template](https://docs.google.com/document/d/1Advwtsv2flH1jvfgNLUn8hC3WXaKpiT_PyCOYgNaj-o/edit?usp=sharing)) with the new team member which they can use for tracking their progress for the first 3 months

/assign me
