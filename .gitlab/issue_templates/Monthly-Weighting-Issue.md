<!-- TODO Add the correct milestone and uncomment this line:
This issue is to capture discussion and feedback on the estimation process for %milestone. 
-->

The process has had changes over the last year and we've also welcomed new product stable counterparts to the team. This will ensure we're aligned and any discrepancies are updated in the handbook.

Please read the [Capacity Planning](https://about.gitlab.com/handbook/engineering/development/dev/plan-product-planning-certify-be/#capacity-planning) section of the ~"group::product planning" Backend team page as it may have changed since last time.

<!--
TODO: Uncomment this line and tag the assignees for this month:

On rotation this month are @username and @username2 :wave: 

-->

<!-- TODO: Check that the following text matches that on the team page -->

> 1. Look through all issues with the ~"workflow::planning breakdown" label, filtered by your group, ~backend and ~"Next Up" (you can use the links in the table below).
> 2. For those they understand, they add a weight. If possible, they also add a short comment explaining why they added that weight, what parts of the code they think this would involve, and any risks or edge cases we'll need to consider.
> 3. For issues that already have a weight check that it's still appropriate for the work required, which may have changed since the effort was last estimated.
> 4. Timebox the issue weighting overall, and for each issue. The process is intended to be lightweight. If something isn't clear what weight it is, they should ask for clarification on the scope of the issue.
> 5. If two people disagree on the weight of an issue, even after explaining their perceptions of the scope, we use the higher weight.

[Issues ready to weight](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Next%20Up&label_name[]=group%3A%3Aproduct%20planning&label_name[]=workflow%3A%3Aplanning%20breakdown&label_name[]=backend)

Note: Please discuss individual issues in the issues themselves, this is just to align on the estimation process.

<!-- TODO: cc the relevant PMs & EMs -->
/cc @donaldcook @mushakov
/due 15th

<!--
TODO: Uncomment this line and add the assignees for this month and the milestone

/assign @username @username2
/milestone %milestone
-->
