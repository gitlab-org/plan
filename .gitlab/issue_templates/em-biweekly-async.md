This issue is a replacement for the Plan EMs weekly sync call. 

Please raise discussion topics you have in the comments; including:

* Ideas
* Requests for help
* Concerns & questions
* Suggestions for collaboration/projects

Please start a new thread for each discussion topic by the end of Tuesday so that
responses can be collected by the usual sync call time on Thursday.

/assign @blabuschagne @donaldcook @johnhope @kushalpandya
/due Thursday
/title EM Biweekly Async Issue
