This is the kickoff issue designed to help us, as a team, understand what each other are working on, what has been committed to at the start of the milestone and to start discussions as early as possible on how to break down large issues.

It doesn't include everything, for that please see the [Plan Kanban] board or either of the two backend boards.

<!-- Update the milestone in this link -->
[Plan Kanban]: https://gitlab.com/groups/gitlab-org/-/boards/1226305?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan&label_name[]=backend&milestone_title=%23started&not[label_name][]=group%3A%3Aproject%20management

## New Deliverables

<!-- replace weight with actual weight or ~"Needs weight" label -->
#### [Link to deliverable] - (weight) - ~"planning priority" / ~direction

Summary of requirements, progress to date and any special circumstances.

## Significant bugs

- [Description of the bug] - (weight)
  - Useful info
  - other


## Backstage / Tech debt / Small bugs

- [Description of work] - **(weight)**

## Continuing work from 12.7

#### [Description of work] - (weight) - ~"planning priority" / ~direction / ~feature

## Other

##### [Stuff not captured by other sections] - (weight) - ~feature 


### Team Availability

| Team member | OOO (from PTO Ninja) | Days  |
|-------------|----------------------|-------|
| Name        | 02.10-02.28          |  8    |
| Total       |                      | xx%   |

<!--

| Team member | OOO (from PTO Ninja) | Days  |
|-------------|----------------------|-------|
| Charlie     |                      |       |
| Felipe      |                      |       |
| Jan         |                      |       |
| Jarka       |                      |       |
| John        |                      |       |
| Total       |                      |       |

or

| Team member | OOO (from PTO Ninja) | Days  |
|-------------|----------------------|-------|
| Alexandru   |                      |       |
| Brett       |                      |       |
| Eugenia     |                      |       |
| Heinrich    |                      |       |
| Mario       |                      |       |
| Patrick     |                      |       |
| Total       |                      |       | 

-->

### Capacity Planning

| Group                | 12.5 | 12.6 | 12.7 | 12.8 |
|----------------------|------|------|------|------|
| Portfolio Management | 27   | 30   | 16   | |
| Certify              | 2    | 10   | 12   | |
| Total                | 29   | 40   | 28   | |

### Exit Criteria

- [ ] All issues weighted
- [ ] Capacity totals added to Capacity Planning
- [ ] Deliverables discussed, broken down and weighted (into ~"workflow::ready for development" where possible)
- [ ] Issues of weight 5 broken down
- [ ] One team member assigned to each ~"planning priority" 
