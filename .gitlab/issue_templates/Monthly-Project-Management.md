# Welcome to Milestone [xx.y]!



## Quick Reference :reminder_ribbon: 

#### Important Dates
Per the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) let's keep the following key dates in mind:

| | |
|:---:|---|
|4th of the Month| -Draft of the issues that will be included in the next released (released 22nd of next month). <br> -Start capacity and technical discussions with engineering/UX.|
|7th of the Month| -Weighing issue is created.|
|13th of the Month| -Weighing issue is closed. <br> -Release scope is finalized. In-scope issues marked with milestone. <br> -Kickoff document is updated with relevant items to be included.|
|16th of the Month| -Group Kickoffs calls recorded and uploaded by the end of the day.<br> -Engineering DRIs are determined and assigned to feature theme work.|

#### Issue Boards

- [Issues by Workflow label](https://gitlab.com/groups/gitlab-org/-/boards/1285239?label_name[]=group%3A%3Aproject%20management)
- [Issues by Milestone](https://gitlab.com/groups/gitlab-org/-/boards/1910149?label_name[]=group%3A%3Aproject%20management)

## Planned Work :construction_worker_tone3: 

#### Release Themes
Instead of a long list of issues/epics we plan to work on, each Plan group will outline 1 - 3 themes to focus on for the Release. Ideally, we will get 1 FE + 1 BE allocated to each theme with the goal of iterating and delivering as much as possible towards that theme.

[explain theme focus]

|Issue|Why|Business Value|
|---|---|---|
|[ ]()| | |
|[ ]()| | |
|[ ]()| | |

#### Work Classification

Features are nice but a stable and well performing app is critical. Let's ensure we have a healthy balance of focus on tech debt, performance and security issues.

Please review the [MR Types dashboard](https://about.gitlab.com/handbook/engineering/metrics/dev/plan/project-management/#mr-types-dashboard). Check off when complete.
  * [ ] @donaldcook (EM)
  * [ ] @gweaver (PM: Project Management)
  * [ ] @dchevalier2 (Quality)
  * [ ] @nickleonard (UX)
  * [ ] @jackib (UX)

|Planned Current+| % of Planned Issues| Link to Backlog|
|---|:---:|---|


#### Kickoff Video (added on 15th of the Month) | [Kickoff Video]()

### Planning Hygiene Checklist

- [ ] One main deliverable plus one spike, or exception described below.
   - _Describe exception here or delete_
- [ ] Deliverables fit within 75% of [3-month rolling average](https://app.periscopedata.com/app/gitlab/587512/Plan-stage-capacity-planning) capacity.

### Planning metrics | [Dashboard](https://app.periscopedata.com/app/gitlab/587512/Plan-stage-capacity-planning)
- Average velocity: 
- Committed weight:

/assign @gweaver @mushakov @donaldcook @dchevalier2 @nickleonard
/label ~"devops::plan" ~"group::project management"

