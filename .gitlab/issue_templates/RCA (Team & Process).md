[Team KPI Dashboard](https://app.periscopedata.com/app/gitlab/536601/Backend-Plan:Portfolio-Management-&-Certify-Development-Metrics)

## Summary

<!-- Short description of what went wrong -->

* Categories(s) affected:
* Team attribution: <!-- e.g. ~group::certify -->
* Minutes downtime or degradation: <!-- e.g. -50% M/M MRs (From 40 in 2020.01 to 20 in 2020.02) -->

<!-- Screenshots here, if any -->

## Impact and Metrics

<!--
  Complete this section during evidence-gathering, while completing the causal
  tree analysis. This way the exact numbers and methodologies are visible.
-->
     
<!-- 
  One strategy is to create a table investigating each measurement at a time and
  determining whether it is correlated with the change observed.
-->

We use [is/isn't](https://quality-one.com/rca/) to determine which metrics are clearly correlated with the problem and which we can discard.

| Metric | Is | Isn't | Reason |
| --- | --- | --- | --- | 
| <!-- e.g. MTTM --> | | <!-- :white_check_mark: --> | <!-- MTTM has been stable relative to earlier in the year --> | 

The outcome of these investigations will help complete the causal factor tree in the next section.

## Causal Factor Tree

<!--
  You can apply the following classes to tree elements as follows:
    - possible_case: worthy of future investigation, or in progress
    - positive: investigated and identified as a root cause
    - negative: investigated but wasn't a cause
    - No class: investigated and identified as a path to a root cause
-->
```mermaid
graph TD
  classDef positive fill:#cec,stroke:#0f0,stroke-width:0px;
  classDef negative fill:#ecc,stroke:#f00,stroke-width:0px;
  classDef possible_cause fill:#cce,stroke:#00f,stroke-width:0px;

  assertion1(Possible cause under investigation) --> outcome[The outcome being investigated]

  assertion2[Partial cause] --> outcome
  a2_test1["Not a cause"] --> assertion2
  a2_test2["Partial cause"] --> assertion2
  a2_test3["Worth future investigation"] --> assertion2
  a2_1_test2("Root cause") --> a2_test2
  a2_1_test1["Not a cause"] --> a2_test2

  class a2_test1,a2_1_test1 negative;
  class assertion1,a2_test3 possible_cause;
  class a2_1_test2 positive;
  
```

## What went well?

## What can be improved?

## Corrective actions

<!--
  Include actions that can be taken, including check boxes. If they can't be
  immediately enacted or are evergreen, perhaps a handbook update or scheduled
  check-in is appropriate
-->

**Objective**
- [x] Action 1
- [ ] Action 2
  - Sub-action 2.1
  - Sub-action 2.2

## Guidelines

- [Blameless RCA Guideline](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/workflows/internal/root-cause-analysis.html)
- [5 Whys](https://en.wikipedia.org/wiki/5_Whys)

/cc `@timzallmann @clefelhocz1` <!-- if appropriate. CC team members also -->
/label ~"Root Cause Analysis"
