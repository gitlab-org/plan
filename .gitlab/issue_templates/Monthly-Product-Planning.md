# Welcome to Milestone [xx.y]!



## Quick Reference :reminder_ribbon: 

#### Important Dates
Per the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) let's keep the following key dates in mind:

| | |
|:---:|---|
|4th of the Month| -Draft of the issues that will be included in the next released (released 22nd of next month). <br> -Start capacity and technical discussions with engineering/UX.|
|12th of the Month| -Release scope is finalized. In-scope issues marked with milestone. <br> -Kickoff document is updated with relevant items to be included.|
|15th of the Month| -Group Kickoffs calls recorded and uploaded by the end of the day.|

#### Issue Boards

- [Issues by Workflow label](https://gitlab.com/groups/gitlab-org/-/boards/1569369?label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Aproduct%20planning&milestone_title=Started)
- [Issues by Milestone](https://gitlab.com/groups/gitlab-org/-/boards/4873470?not[label_name][]=product%20work&not[label_name][]=type%3A%3Aignore&not[label_name][]=UX%20Design&label_name[]=group%3A%3Aproduct%20planning)

## Planned Work :construction_worker_tone3: 

#### Deliverables
These are the issues we are committing to in this milestone. Please read below to understand why these are important!

|Issue|Team|Why?|
|---|---|---|
| | | |
| | | |
| | | |

#### Work Classification

Features are nice but a stable and well performing app is critical. Let's ensure we have a healthy balance of focus on tech debt, performance and security issues.

|Planned Current+| % of Planned Issues| Link to Backlog|
|---|:---:|---|
|[Planned Security Issues¹](https://gitlab.com/groups/gitlab-org/-/boards/4873470?not[label_name][]=product%20work&not[label_name][]=UX&not[label_name][]=Technical%20Writing&label_name[]=group%3A%3Aproduct%20planning&label_name[]=security)| tbd%|[security backlog](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=security&label_name[]=group::product+planning)| 
| [Planned Bug Issues](https://gitlab.com/groups/gitlab-org/-/boards/4873470?not[label_name][]=product%20work&not[label_name][]=UX&not[label_name][]=Technical%20Writing&label_name[]=group%3A%3Aproduct%20planning&label_name[]=type%3A%3Abug)| tbd%| [bug backlog](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=group%3A%3Aproduct%20planning&label_name%5B%5D=type%3A%3Abug&not%5Blabel_name%5D%5B%5D=severity%3A%3A4&first_page_size=20)
| [Planned SUS Impacting Issues](https://gitlab.com/groups/gitlab-org/-/boards/4873470?not[label_name][]=product%20work&not[label_name][]=UX&not[label_name][]=Technical%20Writing&label_name[]=group%3A%3Aproduct%20planning&label_name[]=SUS%3A%3AImpacting)| tbd% | [SUS Impacting backlog](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=group%3A%3Aproduct%20planning&label_name%5B%5D=SUS%3A%3AImpacting&first_page_size=100)|
| [Planned Maintenance Issues](https://gitlab.com/groups/gitlab-org/-/boards/4873470?not[label_name][]=product%20work&not[label_name][]=UX&not[label_name][]=Technical%20Writing&label_name[]=group%3A%3Aproduct%20planning&label_name[]=type%3A%3Amaintenance)| tbd%| [maintenance backlog](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=group%3A%3Aproduct%20planning&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=20)|
| [Planned Feature Issues](https://gitlab.com/groups/gitlab-org/-/boards/4873470?not[label_name][]=product%20work&not[label_name][]=Technical%20Writing&not[label_name][]=UX%20Design&label_name[]=group%3A%3Aproduct%20planning&label_name[]=type%3A%3Afeature)|tbd%| [feature backlog](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=group%3A%3Aproduct%20planning&label_name%5B%5D=type%3A%3Afeature&first_page_size=20)|

_¹Security issues are classified as ~type::bug_

Please review the [MR Types dashboard](https://about.gitlab.com/handbook/engineering/metrics/dev/plan/product-planning/#mr-types-dashboard). Check off when complete.
  * [ ] @kushalpandya (EM)
  * [ ] @amandarueda (PM: Product Planning)
  * [ ] @at.ramya (Quality)
  * [ ] @jackib (UX)

#### Technical [Spike(s)](https://gitlab.com/groups/gitlab-org/-/boards/4873470?not[label_name][]=product%20work&not[label_name][]=type%3A%3Aignore&not[label_name][]=UX%20Design&not[label_name][]=Technical%20Writing&label_name[]=group%3A%3Aproduct%20planning&label_name[]=spike)
1. tbd
1. tbd

## User Experience :pencil: :arrow_right: :computer:

#### Focus Work
We try to focus design efforts each milestone to 1-2 large feature work tasks per milestone. All tasks combined must be at or under a [design weight of 13](https://about.gitlab.com/handbook/product/ux/product-designer/#ux-weight-definitions).

1. tbd
1. tbd

#### Additional Design Work
Along with feature designs, we want to continually prioritize burning down our backlog of UX debt, research, quality of life work, and improving foundational items.

1. tbd
1. tbd
1. tbd

#### Validation Activities
1. tbd
1. tbd

## :beach_umbrella:  Planned Time Off

`@gitlab-org/plan-stage/product-planning` Hey team! :wave_tone2: Please add your planned time off below for this release period. 

_*Note, you do not need to update this table if you take unplanned time off, this is meant to be a quick snapshot of capacity for those who don't have access to capacity planning._

|Team Member|Time Off|
|---|---|
|@kushalpandya | |
|@fguibert | |
|@ramistry| |
|@egrieff | |
|@jprovaznik | |
|@joseph | |
|@nicolasdular|  |
|@nickbrandt |  |
|@amandarueda |  |
|@dteverovsky | |
|@msedlakjakubowski |  |


/assign @mushakov @johnhope @amandarueda @kushalpandya @at.ramya @nickbrandt
/label ~"devops::plan" ~"group::product planning" ~"product work" ~"Planning Issue" ~"type::ignore" 

