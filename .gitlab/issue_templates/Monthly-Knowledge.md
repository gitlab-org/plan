### Release Theme

### Development

- Pages
  - https://gitlab.com/gitlab-org/gitlab/-/issues/389482+ (~backend, ~"type::bug" )
  - https://gitlab.com/gitlab-org/gitlab/-/issues/361016+ (~backend , ~"type::bug" )
  - https://gitlab.com/gitlab-org/gitlab-pages/-/issues/827+ ( ~"type::bug" )
  - https://gitlab.com/gitlab-org/gitlab/-/issues/364127+ (~backend , ~"type::bug" )
- Wiki
  - Reviewing in %"15.10". As of 2023-02-16 in maintenance mode.
- Content Editor
  - https://gitlab.com/gitlab-org/gitlab/-/issues/359008+ (~backend )
  - https://gitlab.com/gitlab-org/gitlab/-/issues/365265+ 
  - https://gitlab.com/gitlab-org/gitlab/-/issues/345072+
  - https://gitlab.com/gitlab-org/gitlab/-/issues/382636+


### PM Timeline (@mmacfarlane)

- [ ] 4th of the Month:
  - [ ] Draft of the issues that will be included in the next release (released 22nd of next month).
  - [ ] Start capacity and technical discussions with engineering/UX.
  - [ ] Start ~"type::bug" discussion with Ramya

- [ ] 12th of the Month:
  - [ ] Release scope is finalized. In-scope issues marked with milestone
  - [ ] Kickoff document is updated with relevant items to be included.

- [ ] 15th of the Month:
  - [ ] Kickoff document finalized.

### EM Tasks

See [product development
timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline):

- [ ] `By 5th` Open weighting issue and ask for estimations from the team
  ([example](https://gitlab.com/gitlab-org/plan/-/issues/243))
- [ ] `By 12th` Complete the [estimation process](https://about.gitlab.com/handbook/engineering/development/dev/plan/knowledge/#capacity-planning), gather final weights
- [ ] `By 12th` Suggest performance, tech debt & bugs (especially check those close to [missing
  SLO](https://app.periscopedata.com/app/gitlab/587512/Plan-stage-capacity-planning?widget=13864633&udv=949595))
- [ ] `By 12th` Use the build board to estimate carry-over (total weight left of ~"workflow::in dev"
  & half of ~"workflow::in dev" is usually a good start)
- [ ] `By 12th` Estimate capacity by taking an average of the last ~3 months (See: [Historical
  capacity](https://about.gitlab.com/handbook/engineering/development/dev/plan/certify/#historical-capacity))
- [ ] `By 14th` Work with PM to fit scope to available capacity (considering carry-over)
- [ ] `By 17th` @mmacfarlane & @johnhope complete the [Planning Hygiene
  Checklist](#planning-hygiene-checklist)
- [ ] `18th` Kickoff!
- [ ] Gather retrospective feedback on estimation from Engineers and PMs
- [ ] Make any updates to team page/process & close issue

### Planning Hygiene Checklist

- [ ] One main deliverable plus one spike, or exception described below.
   - _Describe exception here or delete_
- [ ] Deliverables fit within 75% of [3-month rolling average](https://app.periscopedata.com/app/gitlab/587512/Plan-stage-capacity-planning) capacity.

### Work Type Classification

Please review the [MR Types
dashboard](https://about.gitlab.com/handbook/engineering/metrics/dev/plan/product-planning/#mr-types-dashboard).
Check off when complete.
  * [ ] @johnhope (EM)
  * [ ] @mmacfarlane (PM: Editor)
  * [ ] @at.ramya (Quality)
  * [ ] TBD (UX)

CC: @kassio @ngala @himkp

/assign @mmacfarlane @johnhope @at.ramya
/label ~"section::dev" ~"devops::plan" ~"group::knowledge" ~"Planning Issue"
