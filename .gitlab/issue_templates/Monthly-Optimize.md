**Table of Contents**

[[_TOC_]]

## Important Dates to track against:

Per the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/development/dev/manage/optimize/#planning) let's keep the following key dates in mind:

4th of the Month:

* A board filtered by the Next 1 - 3 releases milestone should be populated with upcoming issues.
* Start capacity and technical discussions with engineering/UX.

12th of the Month:

* Issues that we know will slip from the previous release should be reweighted for the remaining effort left and rescheduled to the next release.

15th of the Month:

* Depending on availibility, either Product or Engineering will take capacity into consdieration and assign the top issues in each [type category](https://about.gitlab.com/handbook/engineering/development/dev/manage/optimize/#merged-merge-request-types) to the next release.

## Boards

<details><summary>Different boards we use for planning and organization</summary>

- [Upcoming +1 board](https://gitlab.com/groups/gitlab-org/-/boards/4290510??label_name[]=group%3A%3Aoptimize&milestone_title=XX.X)
- [Next 1-3 Board](https://gitlab.com/groups/gitlab-org/-/boards/4401884?milestone_title=Next%201-3%20releases&label_name[]=group%3A%3Aoptimize )
- [Build board](https://gitlab.com/groups/gitlab-org/-/boards/1401511?label_name[]=group%3A%3Aoptimize&milestone_title=XX.X)
- Error budgets - [Grafana](https://dashboards.gitlab.net/d/stage-groups-optimize/stage-groups-group-dashboard-manage-optimize?orgId=1)

</details>

## Capacity / OOO

Milestone dates: **XXXX-XX-XX** to **XXXX-XX-XX**

-  BE: 
-  FE: 
-  UX
-  PM

## Objectives & Themes

1. Increase Optimize group pages monthly active users (GMAU) 190K by adding Customizable Analytics Dashboard:

2. [VSM category maturity](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1970) by usability improvements: 

- https://gitlab.com/gitlab-org/gitlab/-/issues/378373+

3. Continue progress on DORA 4 Metrics📈:

4. Maintenance & Bugs 🐛:

5. Validation and design issues for the release:

- XXX

## GitLab Product Themes
<details><summary>Click to expand</summary>

### Themes
 🏎 Performance * 🔒 Security * 😍 Usability* 📈 Customer Requests * 🏆 OKR * 🐛 Bug

* [GitLab Hosted First](https://about.gitlab.com/direction/#gitlab-hosted-first) :cloud: 
* [Improve Key workflow usability](https://about.gitlab.com/direction/#improve-key-workflow-usability) :key: 
* [Extend our lead in CI/CD](https://about.gitlab.com/direction/#extend-our-lead-in-cicd) :medal: 

</details>

/assign @hsnir1 @blabuschagne @lvanc
/label ~"section::dev" ~"devops::plan" ~"group::optimize" ~"Planning Issue"
