## Important dates to track against

Per the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) let's keep the following key dates in mind:

4th of the Month:

* Draft of the issues that will be included in the next released (released 22nd of next month).
* Start capacity and technical discussions with engineering/UX.

9th of the Month:

* All potential issues in consideration for being included in the upcoming milestone are weighted and refined.

12th of the Month:

* Release scope is finalized. In-scope issues marked with milestone
* Kickoff document is updated with relevant items to be included.

15th of the Month:

* Group Kickoffs calls recorded and uploaded by the end of the day.
* All issues in the upcoming milestone have a `workflow::` label. (DRI: @gweaver)
* All issues in the upcoming milestone have either the ~Deliverable or ~Stretch label applied to them (DRI: Backend - @jlear, Frontend - @donaldcook)

## Strategic Context

FY22 Product Investment Themes:

- [Adoption through usability](https://about.gitlab.com/direction/#adoption-through-usability)
- [GitLab-hosted first](https://about.gitlab.com/direction/#gitlab-hosted-first)

Quarterly R&D Themes:

  -
  -

OKRs for this quarter:

- Object: Provide the next generation "planning object" that is usable, performant, and scales with our customers.

  - KR1:
  - KR2:
  - KR3:

<br>

- Objective: Improve the usability, reliability, and performance of the Plan:Project Management surface area.

  - KR1:
  - KR2: 
  - KR3:

## Capacity

| Avg. Velocity | Current Weight In Milestone | Diff |
| ------------- | -------------- | ---- |
|               |                | {+   +} |

| Avg. Issues Closed | Current Issues In Milestone | Diff |
| ------------- | -------------- | ---- |
|               |                | {+   +} |

## Release Plan Themes

### Direction

Items within this theme relate to either moving our Project Management direction forward or following through on a hard committment to a customer including issues tagged as ~direction, ~"type::feature", ~"feature::addition", and ~"planning priority". All team members are encouraged to propose items to be included within this theme for the upcoming release.

- [Issue](1)
- [Issue](2)
- [Issue](3)

### Improve Product Experience
 
Items within this theme relate to improving the overall usability of the product including issues tagged as ~"UX debt", ~"Actionable Insight", ~Dogfooding, ~customer, ~"feature::enhancement". All team members are encouraged to propose items to be included within this theme for the upcoming release. 

- [Issue](1)
- [Issue](2)
- [Issue](3)

### Quality

Items within this theme relate to the overall quality and performance of our codebase and infrastructure including issues tagged as ~"Mechanical Sympathy", ~security, ~performance, ~availability , ~"type::bug", ~"type::maintenance", ~"type::tooling", ~"support request",   and error budgets. All team members are encouraged to propose items to be included within this theme for the upcoming release. 

- [Issue](1)
- [Issue](2)
- [Issue](3)

/assign @gweaver @donaldcook @nickleonard @dchevalier2 
/label ~"devops::plan" ~"group::project management" 
