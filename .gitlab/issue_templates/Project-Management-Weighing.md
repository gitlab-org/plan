The purpose of this issue is to simulate a sprint planning meeting. Please ask any questions about the features in the threads below. Add an emoji to the discussion with what you think the weight should be. Let's keep it lightweight for now, and try to timebox just a few minutes to thinking through each feature.

We do want weights to eventually align more with complexity, but it may be easier to tie it to time for now. Let's go with the following scale (in dev time):

* :one: = couple of hours
* :two: = couple days
* :three: = week
* :four: = two weeks
* :five: = month

Please let me know if you have any questions. Remove yourself as an assignee after you've added weights.

### Important dates to keep on track with planning.
* [x] 4th of the month - This issue created.
* [ ] 7th of the month - Threads created around planned milestone issues by EM. Issue assigned to engineers on team.
* [ ] 14th of the month - Weighing completed in this issue. EM to transfer weights to issues.

/cc @gweaver
/assign @donaldcook
